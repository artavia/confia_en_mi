# Confia en Mi

## Please visit
You can see [the finished work at gitlab.io](https://artavia.gitlab.io/confia_en_mi "the finished work at gitlab") in either English or Spanish.

## In loving memory of my baby sister, Cristy
Cristy had suffered from brain cancer since January 2015. And while she is greatly missed by myself and others it is but for a season. I want to tell you that she gave her life to the Lord Jesus Christ&#45;&#45; the King of kings; the Lord of lords. She delivered her life unto Jesus, so that He&#45;&#45; whose promises do not return void &#45;&#45;receives her after this life. Therefore, she dies only ONCE by his grace and mercy and she will rejoice in the presence of the Lord God, Yehovah, and God's only begotten son, the sinless and stainless Lamb of God, Jesus Christ&#45;&#45; the name above all names. In short, she is now safe in the bosom of the Lord under his protection where she is far from the dangers of this world that grow by the day.

![Cristy in October 2019](https://gitlab.com/artavia/confia_en_mi/-/raw/master/markdown_assets/cristy.png)

Before she departed from us and went to be with the Lord on January 24th, 2020, she had a peace about her that I had not seen in a very long time (ever since she was a little child). 

In her final days, she realized that all the things of this world are vanity in contrast to the gift (that we do not have to pay back) that Jesus&#45;&#45; the Messiah, the savior to the world &#45;&#45;has given to each and every person who is alive on this earth. More importantly, she confidently assured me that &quot;I will see you again in the presence of our Lord Jesus.&quot; 

You can imagine the joy I felt in hearing that bold statement from her. I was shedding tears of sadness moments before and because of her declaration, I smiled from ear to ear with relief as I, then, began to shed tears of joy! All praise be to God!

## Attributions
  - This dedication is based on an anonymous work called "Confia en Mi." 
  - My brother in Christ, Jan Boshoff (a\k\a [FinalCall07](https://www.youtube.com/watch?v=XqeHKZZ6Er8 "Link to 'Be Persistent in Prayer'")) before he, too, went to be with our Lord Jesus in heaven on January 20, 2020, had posted a video lesson based on **The Gospel of Luke 11: 5&#45;13**. When I see Jan in the presence of the Lord, I will personally thank him.
  - You can visit DuckDuckGo and conduct a search on &quot;how to minister to dying non-believer&quot;.

# All praise and glory be to the Lord, Jesus Christ
It is God's will that none of his children perish in darkness once we depart this world. If any of you are dealing with a terminally ill person who is unsaved, I want to recommend some passages from the Holy Bible that can be of help. I recommend that you read **before** and **after** each passage so that you can enjoy the full context of each message. These are the words of life, ladies and gentlemen: 

  - Gospel of Luke 11: 5&#45;13 (The persistent and stubborn friend at midnight)
  - Gospel of Luke 15: 1&#45;10 (The joy that God's angels show when a sinner is converted)
  - Gospel of Luke 15: 11&#45;32 (The prodigal son)

  - Gospel of John 1:12&#45;13 (He gave them power to be sons of God)
  - Gospel of John 3:5&#45;8 (Being born again)

  - Romans 1: 16&#45;17 (the just will live by faith)
  - Romans 8: 14&#45;17 (the sons of God)

  - 1 Corinthians 1: 8 (God is faithful)
  
  - 2 Corinthians 1: 21&#45;22 (Jesus is anointed by God )
  - 2 Corinthians 5: 15&#45;17 (A new creation)

  - 2 Peter 1: 16&#45;18 (We ourselves heard the voice of God come from heaven)

  - 1 John 3: 9 (A creation of God will avoid sin)
  - 1 John 4: 7&#45;8 (Those who love are of God and know God)
